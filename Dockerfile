FROM debian:stable

ARG PHP_VERSION

RUN useradd -mUG sudo app \
 && echo 'app:app' | chpasswd \
 && chsh -s /bin/bash app \
 && mkdir /home/app/docroot

RUN apt-get update \
  && apt-get install -y \
    sudo \
    lsb-release \
    ca-certificates \
    apt-transport-https \
    software-properties-common \
    gnupg2 \
    wget \
    locales \
  && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/sury-php.list \
  && wget -qO - https://packages.sury.org/php/apt.gpg | apt-key add - \
  && locale-gen en_GB.UTF-8 \
  && locale-gen en_US.UTF-8 \
  && localedef -f UTF-8 -i en_GB en_GB.UTF-8 \
  && localedef -f UTF-8 -i en_GB en_GB \
  && localedef -f UTF-8 -i en_US en_US.UTF-8 \
  && localedef -f UTF-8 -i en_US en_US \
  && localedef -f UTF-8 -i ar_AE ar_AE.UTF-8 \
  && localedef -f UTF-8 -i ar_AE ar_AE \
  && localedef -f UTF-8 -i th_TH th_TH.UTF-8 \
  && localedef -f UTF-8 -i th_TH th_TH

RUN apt-get update \
 && apt-get -y install \
    lsb-base \
    curl \
    zip \
    unzip \
    gettext \
    php${PHP_VERSION} \
    php${PHP_VERSION}-common \
    php${PHP_VERSION}-cli \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-mysql \
    php${PHP_VERSION}-pgsql \
    php${PHP_VERSION}-xml \
    php${PHP_VERSION}-mbstring \
    php${PHP_VERSION}-curl \
    php${PHP_VERSION}-zip \
    php${PHP_VERSION}-intl \
    php${PHP_VERSION}-imagick \
    php${PHP_VERSION}-gd \
    php${PHP_VERSION}-gettext \
    php${PHP_VERSION}-dom \
    php${PHP_VERSION}-exif \
    php${PHP_VERSION}-fileinfo \
    php${PHP_VERSION}-bcmath \
    php${PHP_VERSION}-iconv \
    php${PHP_VERSION}-mcrypt \
    php${PHP_VERSION}-simplexml \
    php${PHP_VERSION}-xmlreader \
    php${PHP_VERSION}-ssh2 \
    php${PHP_VERSION}-ftp \
    php${PHP_VERSION}-gearman \
    php${PHP_VERSION}-interbase \
    php${PHP_VERSION}-ctype \
    php${PHP_VERSION}-pdo \
    php${PHP_VERSION}-tokenizer \
    php${PHP_VERSION}-pdo \
    php${PHP_VERSION}-pdo-mysql \
    php${PHP_VERSION}-pdo-pgsql \
    php${PHP_VERSION}-amqp \
    php${PHP_VERSION}-xmlrpc \
    php${PHP_VERSION}-soap \
    php${PHP_VERSION}-mongo \
    php${PHP_VERSION}-redis \
    php${PHP_VERSION}-mongo \
    php${PHP_VERSION}-sybase

RUN sed -i -- 's/www-data/app/g' /etc/php/${PHP_VERSION}/fpm/pool.d/www.conf \
 && sed -i -- 's/listen[[:space:]]*=[[:space:]]*.*/listen = 0.0.0.0:8080/g' /etc/php/${PHP_VERSION}/fpm/pool.d/www.conf

RUN echo "root ALL=(ALL:ALL) ALL" > /etc/sudoers \
 && echo "app  ALL=(ALL:ALL) NOPASSWD: /usr/sbin/service php${PHP_VERSION}-fpm start" >> /etc/sudoers \
 && echo "@includedir /etc/sudoers.d" >> /etc/sudoers

USER app
WORKDIR /home/app/docroot

ENV PHP_VERSION=${PHP_VERSION}

CMD sudo /usr/sbin/service php${PHP_VERSION}-fpm start \
 && sleep infinity
