# php

## Introduction
This project aims to ease the struggles of the dockerized development for beginner users. The built images are too big
to serve on servers. And also these are too monolithic to use in a professional environment for a long term period.
The author of this project suggests using it only as a kickstarter of your project, and when you're familiarized with
the requirements of the project, created a better fitting one or simply just configre and use the official php images.

## Tags and Php versions
The Dockerfile is built with the following tags, and each one is representing a php version (except latest, wich is the
latest stable php version):
* 5.6
* 7.0
* 7.1
* 7.2
* 7.3
* 7.4
* 8.0
* 8.1
* 8.2
* 8.3

## Architectures
`docker buildx` provides cross platform build possibilities. So with this project I try to provide compatibility to the
new M1 mac processors as well. The currently supported architecture list:
* amd64
* 386
* arm64

## Php extensions
Tried to collect the most commonly used php extensions. The followings are installed and enabled:
* amqp
* bcmath
* cli
* common
* ctype
* curl
* dom
* exif
* fileinfo
* fpm
* ftp
* gd
* gearman
* gettext
* iconv
* imagick
* interbase
* intl
* mbstring
* mcrypt
* mongo
* mysql
* pdo
* pdo-mysql
* pdo-pgsql
* pgsql
* redis
* simplexml
* soap
* ssh2
* sybase
* tokenizer
* xml
* xmlreader
* xmlrpc
* zip

## Users and rights
Should mention two users: `root` and `app`. The root is quite conventional.

However, the `app` user is the "working" user.
This is the default user of the image. If you run `docker exec -it {container_name} bash` e.g. , this will be your user,
and also this is the user which starts the `fpm` service and by default the fpm is configured to run as app:app as well.
This is a quite unconventional, but also a cheap solution to avoid file right issues during the development process.

## Configuration
Currently, there's no standard environment variable to configure the container. So unfortunately it requires to create a
standard php conf file. To make it easy, I suggest to first copy out the existing one from the container:

`docker cp {container_name}:/etc/php/{PHP_VERSION}/fpm/pool.d/www.conf default.conf`

And then just simply share the modified one via a volume, like

```yaml
    volumes:
      - ./modified.conf:/etc/php/{PHP_VERSION}/fpm/pool.d/www.conf
```

By default, the fpm listens on 8080 port inside the container.
To make it work with nginx, use a cgi pass:
```
fastcgi_pass {FPM_CONTAINER_IP_ADDRESS}:8080;
```
